const express = require('express');
const nanoid = require('nanoid');
const Link = require('../models/Link');

const router = express.Router();

const createRouter = () => {

  router.post('/links', (req, res) => {
    const link = new Link(req.body);
    link.shortUrl = nanoid(7);

    // Link.findOne({shortUrl: url}).then(result => {
    //   if (!result) {
    //     link.save()
    //     .then(result => res.send(result))
    //     .catch(error => res.status(400).send(error));
    //   } else {
    //     res.sendStatus(400);
    //   }
    // });

    link.save()
    .then(result => res.send(result))
    .catch(error => res.status(400).send(error));

  });

  router.get('/:id', (req, res) => {
    const link = req.params.id;
    Link.findOne({shortUrl: link})
    .then(result => {
      if (result) {
        res.status(301).redirect(result.originalUrl);
      }
      else {
        res.sendStatus(404);
      }
    }).catch(() => res.sendStatus(500));
  });

  return router;
};

module.exports = createRouter;